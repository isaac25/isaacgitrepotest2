public without sharing class A2A_AccountTriggerHandler extends AbstractTriggerHandler {

  protected List<Account> oldAccounts = new List<Account>();
  protected List<Account> newAccounts = new List<Account>();
  protected Map<Id, Account> oldAccountsByIds = new Map<Id, Account>();
  protected Map<Id, Account> newAccountsByIds = new Map<Id, Account>();

  private static final String ACCOUNT_KEY = 'Account';
  private static final String SUBACCOUNT_KEY = 'SubAccount';
  private static final Set<String> PARENT_ACCOUNT_RECORD_TYPES = new Set<String> {
      Constants.ACCOUNT_RECORD_TYPE_NATIONAL_ACCOUNT,
      Constants.ACCOUNT_RECORD_TYPE_AGENCY_ACCOUNT};

  private Map<Id, Map<Id, Id>> subAccountIdToChangedOwnerPairs;

  private Map<Id, Map<Id, Id>> subAccountIdToChangedServiceRepPairs;
  private Map<Id, Map<Id, Id>> subAccountIdToChangedStrategicRepPairs;
  private Map<Id, Map<Id, Id>> subAccountIdToChangedAgencyRepPairs;
  private Map<Id, Map<Id, Id>> subAccountIdToChangedSalesRepPairs;

  private Map<Id, Map<Id, Id>> subAccountIdToChangedAllRepPairs;

  private Map<Id, Map<Id, Id>> subAccountIdToChangedContactsPair;
  private Map<Id, Id> newContactIdToSubAccountOwnerId = new Map<Id, Id>();

  private Map<Id, Map<String, String>> subAccountIdToChangedCurrencyPairs;

  private OpportunityLineItemUtility opportunityLineItemUtility = new OpportunityLineItemUtility();

  private static final Id nationalSubAccountRecTypeId = Utility.recordTypeId('Account', Constants.ACCOUNT_RECORD_TYPE_NATIONAL_SUB_ACCOUNT);
  private static final Id agencySubAccountRecTypeId = Utility.recordTypeId('Account', Constants.ACCOUNT_RECORD_TYPE_AGENCY_SUB_ACCOUNT);

  // In unit tests, we will inject a mocked instance to bypass Future, Callout, etc.
  @TestVisible
  public static SoloService.SoloServiceInterface soloServiceInstance = new SoloService();

  public A2A_AccountTriggerHandler(List<Account> oldAccounts, List<Account> newAccounts, Map<Id, Account> oldAccountsByIds, Map<Id, Account> newAccountsByIds) {
    this.oldAccounts = oldAccounts;
    this.newAccounts = newAccounts;
    this.oldAccountsByIds = oldAccountsByIds;
    this.newAccountsByIds = newAccountsByIds;
  }

  public override void onBeforeInsert() {
    Map<String, Account[]> newSplittedAccounts = splitAccountsAndSubAccounts(newAccounts);
    Account[] newSubAccounts = newSplittedAccounts.get(SUBACCOUNT_KEY);
    for (Account newAccount : newSubAccounts) {
      newAccount.a2a_Account_Audited__c = false;
    }
    updateOwnersForSubAccounts(newSubAccounts);
    linkSubAccountToParentAccountIfExists(newSubAccounts);
  }

  public override void onAfterInsert() {
    A2A_AccountTeamMemberManagement.createAccountTeams(newAccounts);
    Map<String, Account[]> newSplittedAccounts = splitAccountsAndSubAccounts(newAccounts);

    Account[] newSubAccounts = newSplittedAccounts.get(SUBACCOUNT_KEY);
    afterInsertInit(newSubAccounts);

    updateOwnersForRelatedObjectsAfterInsert();
    updateParentAccountOwnerCheckbox(newAccounts, null);

    syncCreatedRecordIdToAdcIfNotCreatedByIntegration();

    Account[] newParentAccounts = newSplittedAccounts.get(ACCOUNT_KEY);
    updateUnlinkedSubAccountsAndLeadsToLinkToNewParentAccount(newParentAccounts);
  }

  private void syncCreatedRecordIdToAdcIfNotCreatedByIntegration() {
    Profile currentProfile = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
    if (currentProfile.Name.equals(Constants.INTEGRATE_PROFILE_NAME)) {
      return;
    }
    List<Id> accountIds = new List<Id>();
    for (Account newAccount : newAccounts) {
      if (!String.isBlank(newAccount.a2a_AdCentralAdvertiserId__c) || !String.isBlank(newAccount.AdCentralProspectId__c)) {
        accountIds.add(newAccount.Id);
      }
    }
    if (accountIds.isEmpty()) {
      return;
    }
    sendAccountsBulkCreateEventToSolo(accountIds);
  }

  /**
   * Contains @Future callout to Solo service
   *
   * @param accountIds
   */
  private void sendAccountsBulkCreateEventToSolo(List<Id> accountIds) {
    soloServiceInstance.sendAccountsBulkCreateEvent(accountIds);
  }

  public override void onBeforeUpdate() {
    Map<String, Account[]> newSplittedAccounts = splitAccountsAndSubAccounts(newAccounts);
    Account[] updatedSubAccounts = newSplittedAccounts.get(SUBACCOUNT_KEY);
    if (isTransferRecordPermissionEnabled()) {
      updateOwnersForSubAccounts(updatedSubAccounts);
    }
    linkSubAccountToParentAccountIfExists(updatedSubAccounts);
  }

  public override void onAfterUpdate() {
    Map<String, Account[]> updatedSplittedAccounts = splitAccountsAndSubAccounts(newAccounts);
    Account[] updatedSubAccounts = updatedSplittedAccounts.get(SUBACCOUNT_KEY);

    Map<String, Account[]> oldSplittedAccounts = splitAccountsAndSubAccounts(oldAccounts);
    Account[] oldSubAccounts = oldSplittedAccounts.get(SUBACCOUNT_KEY);

    Map<Id, Account> oldSubAccountsMap = new Map<Id, Account>(oldSubAccounts);

    updateAccountTeamMembers(newAccounts, oldAccountsByIds);
    afterUpdateInit(updatedSubAccounts, oldSubAccountsMap);
    updateOwnersForRelatedObjectsAfterUpdate(updatedSubAccounts);
    updateCurrencyForRelatedOpportunitiesAfterUpdate();
    updateOpportunityReps(updatedSubAccounts, oldSubAccountsMap);
    updateParentAccountOwnerCheckbox(newAccounts, oldAccountsByIds);
  }

  public override void onBeforeDelete() {
    List<Id> deletedAccountIds = new List<Id>();

    for (Account deletedSubAccount : oldAccounts) {
      deletedAccountIds.add(deletedSubAccount.Id);
    }
    removeAccountsWithLeads([SELECT Id FROM Lead WHERE LeadsAccount__c IN :deletedAccountIds]);
  }

  private void removeAccountsWithLeads(List<Lead> associatedLeads) {
    for (Lead associatedLead : associatedLeads) {
      associatedLead.put('LeadAccount__c', null);
    }

    update associatedLeads;
  }

  private void updateOwnersForSubAccounts(Account[] accounts) {
    for (Account accountRecord : accounts) {
      if (accountRecord.a2a_Is_Ad_Agency__c && accountRecord.a2a_Agency_Rep__c != null) {
        accountRecord.OwnerId = accountRecord.a2a_Agency_Rep__c;
      } else if (accountRecord.a2a_Sales_Rep__c != null) {
        accountRecord.OwnerId = accountRecord.a2a_Sales_Rep__c;
      }
    }
  }

  private Boolean isTransferRecordPermissionEnabled() {
    return isProfileHavingTransferRecordPermission() || isUserHavingTransferRecordPermission();
  }

  private Boolean isProfileHavingTransferRecordPermission() {
    return [SELECT Id, PermissionsTransferAnyEntity
            FROM Profile
            WHERE Id = :UserInfo.getProfileId()].PermissionsTransferAnyEntity;
  }

  private Boolean isUserHavingTransferRecordPermission() {
    return [SELECT Id FROM PermissionSetAssignment
            WHERE PermissionSet.Label = 'Transfer Records'
            AND AssigneeId = :UserInfo.getUserId()].size() > 0;
  }

  private Map<String, Account[]> splitAccountsAndSubAccounts(Account[] accounts) {
    Account[] nonSubAccounts = new Account[]{};
    Account[] subAccounts = new Account[]{};
    Map<String, Account[]> splittedAccounts = new Map<String, Account[]>{
        ACCOUNT_KEY => nonSubAccounts,
        SUBACCOUNT_KEY => subAccounts
    };
    for (Account newAccount : accounts) {
      if (isSubAccountRecordType(newAccount) || !String.isBlank(newAccount.a2a_AdCentralAdvertiserId__c)) {
        subAccounts.add(newAccount);
      } else {
        nonSubAccounts.add(newAccount);
      }
    }

    return splittedAccounts;
  }

  private Boolean isSubAccountRecordType(Account record) {
    return record.RecordTypeId ==  nationalSubAccountRecTypeId ||
        record.RecordTypeId == agencySubAccountRecTypeId;
  }

  private void linkSubAccountToParentAccountIfExists(List<Account> subAccounts) {
    Set<String> newParentCompanyIds = new Set<String>();
    List<Account> subAccountsWithChangedParent = new List<Account>();
    for (Account newSubAccount : subAccounts) {
      if (newSubAccount.RecordTypeId.equals(agencySubAccountRecTypeId)) {
        // Agency Sub Account is a bit complicated so we still rely on Integration to set Parent ID directly and let
        // trigger code stand on the side way
        continue;
      }

      Account oldSubAccount = (oldAccountsByIds == null) ? null : oldAccountsByIds.get(newSubAccount.Id);
      if ((oldSubAccount == null || oldSubAccount.Parent_AdCentralParentCompanyId__c != newSubAccount.Parent_AdCentralParentCompanyId__c)
          && !String.isBlank(newSubAccount.Parent_AdCentralParentCompanyId__c)) {
        newParentCompanyIds.add(newSubAccount.Parent_AdCentralParentCompanyId__c);
        subAccountsWithChangedParent.add(newSubAccount);
      } else if ((oldSubAccount != null && !String.isBlank(oldSubAccount.Parent_AdCentralParentCompanyId__c))
          && String.isBlank(newSubAccount.Parent_AdCentralParentCompanyId__c)) {
        // if Parent_AdCentralParentCompanyId__c is changed to empty (e.g. when it is an orphan advertiser in ADC),
        // set ParentId to null
        newSubAccount.ParentId = null;
      }
    }

    if (newParentCompanyIds.isEmpty()) {
      return;
    }

    Map<String, Account> parentCompanyIdToAccountMap = new Map<String, Account>();
    for (Account parentAccount : [
        SELECT Id, AdCentralParentCompanyId__c
        FROM Account
        WHERE RecordType.Name IN :PARENT_ACCOUNT_RECORD_TYPES
        AND AdCentralParentCompanyId__c IN :newParentCompanyIds
    ]) {
      parentCompanyIdToAccountMap.put(parentAccount.AdCentralParentCompanyId__c, parentAccount);
    }

    for (Account subAccount: subAccountsWithChangedParent) {
      if (!String.isBlank(subAccount.Parent_AdCentralParentCompanyId__c)
          && parentCompanyIdToAccountMap.containsKey(subAccount.Parent_AdCentralParentCompanyId__c)) {
        Account parentAccount = parentCompanyIdToAccountMap.get(subAccount.Parent_AdCentralParentCompanyId__c);
        subAccount.ParentId = parentAccount.Id;
      } else if (!String.isBlank(subAccount.Parent_AdCentralParentCompanyId__c)
          && !parentCompanyIdToAccountMap.containsKey(subAccount.Parent_AdCentralParentCompanyId__c)) {
        // if the Parent Account doesn't exist yet, set ParentId to null
        subAccount.ParentId = null;
      }
    }
  }

  private void updateUnlinkedSubAccountsAndLeadsToLinkToNewParentAccount(List<Account> parentAccounts) {
    Map<String, Account> parentCompanyIdToParentAccount = new Map<String, Account>();
    for (Account parentAccount : parentAccounts) {
      if (!String.isBlank(parentAccount.AdCentralParentCompanyId__c)) {
        parentCompanyIdToParentAccount.put(parentAccount.AdCentralParentCompanyId__c, parentAccount);
      }
    }

    Set<String> newParentCompanyIds = parentCompanyIdToParentAccount.keySet();
    if (newParentCompanyIds.isEmpty()) {
      return;
    }

    List<Account> affectedSubAccounts = [
        SELECT Id, Parent_AdCentralParentCompanyId__c, ParentId
        FROM Account
        WHERE RecordType.Name = :Constants.ACCOUNT_RECORD_TYPE_NATIONAL_SUB_ACCOUNT
        AND Parent_AdCentralParentCompanyId__c IN :newParentCompanyIds];

    if (!affectedSubAccounts.isEmpty()) {
      for (Account subAccount : affectedSubAccounts) {
        subAccount.ParentId = parentCompanyIdToParentAccount.get(subAccount.Parent_AdCentralParentCompanyId__c).Id;
      }
      update affectedSubAccounts;
    }

    List<Lead> affectedLeads = [
        SELECT Id, AdCentralParentCompanyId__c, LeadsAccount__c FROM Lead
        WHERE AdCentralParentCompanyId__c IN :newParentCompanyIds];
    if (!affectedLeads.isEmpty()) {
      for (Lead theLead : affectedLeads) {
        theLead.LeadsAccount__c = parentCompanyIdToParentAccount.get(theLead.AdCentralParentCompanyId__c).Id;
      }
      update affectedLeads;
    }
  }

  private void afterInsertInit(Account[] newSubAccounts) {
    subAccountIdToChangedOwnerPairs = new Map<Id, Map<Id, Id>>();

    subAccountIdToChangedServiceRepPairs = new Map<Id, Map<Id, Id>>();
    subAccountIdToChangedSalesRepPairs = new Map<Id, Map<Id, Id>>();
    subAccountIdToChangedStrategicRepPairs = new Map<Id, Map<Id, Id>>();
    subAccountIdToChangedAgencyRepPairs = new Map<Id, Map<Id, Id>>();

    for (Account newSubAccount : newSubAccounts) {
      Map<Id, Id> repPairsOwnerOnly = new Map<Id, Id>();

      Map<Id, Id> repPairsServiceRepOnly = new Map<Id, Id>();
      Map<Id, Id> repPairsSalesRepOnly = new Map<Id, Id>();
      Map<Id, Id> repPairsStrategicRepOnly = new Map<Id, Id>();
      Map<Id, Id> repPairsAgencyRepOnly = new Map<Id, Id>();

      repPairsOwnerOnly.put(newSubAccount.OwnerId, newSubAccount.OwnerId);

      repPairsServiceRepOnly.put(null, newSubAccount.a2a_Service_Rep__c);
      repPairsSalesRepOnly.put(null, newSubAccount.a2a_Sales_Rep__c);
      repPairsStrategicRepOnly.put(null, newSubAccount.a2a_Strategic_Rep__c);
      repPairsAgencyRepOnly.put(null, newSubAccount.a2a_Agency_Rep__c);

      subAccountIdToChangedOwnerPairs.put(newSubAccount.Id, repPairsOwnerOnly);

      subAccountIdToChangedServiceRepPairs.put(newSubAccount.Id, repPairsServiceRepOnly);
      subAccountIdToChangedSalesRepPairs.put(newSubAccount.Id, repPairsSalesRepOnly);
      subAccountIdToChangedStrategicRepPairs.put(newSubAccount.Id, repPairsStrategicRepOnly);
      subAccountIdToChangedAgencyRepPairs.put(newSubAccount.Id, repPairsAgencyRepOnly);
    }
  }

  private void updateOwnersForRelatedObjectsAfterInsert() {
    update A2A_SubAccountTriggerOwnerChangeLists.getOpportunitiesToUpdate(subAccountIdToChangedOwnerPairs);
    update A2A_SubAccountTriggerOwnerChangeLists.getCasesToUpdate(subAccountIdToChangedSalesRepPairs);
  }

  private void afterUpdateInit(Account[] updatedSubAccounts, Map<Id, Account> oldSubAccountsMap) {
    subAccountIdToChangedOwnerPairs = new Map<Id, Map<Id, Id>>();
    subAccountIdToChangedServiceRepPairs = new Map<Id, Map<Id, Id>>();
    subAccountIdToChangedStrategicRepPairs = new Map<Id, Map<Id, Id>>();
    subAccountIdToChangedSalesRepPairs = new Map<Id, Map<Id, Id>>();
    subAccountIdToChangedAgencyRepPairs = new Map<Id, Map<Id, Id>>();
    subAccountIdToChangedAllRepPairs = new Map<Id, Map<Id, Id>>();
    subAccountIdToChangedContactsPair = new Map<Id, Map<Id, Id>>();
    newContactIdToSubAccountOwnerId = new Map<Id, Id>();
    subAccountIdToChangedCurrencyPairs = new Map<Id, map<String, String>>();

    for (Account updatedSubAccount : updatedSubAccounts) {
      Account oldSubAccount = oldSubAccountsMap.get(updatedSubAccount.Id);

      if (null == oldSubAccount) {
        continue;
      }

      // For Unconverted Leads and Open Opportunities;
      Map<Id, Id> repPairsOwnerOnly = new Map<Id, Id>();
      // For Cases and account team member;
      Map<Id, Id> repPairsServiceRepOnly = new Map<Id, Id>();
      // For account team member;
      Map<Id, Id> repPairsStrategicRepOnly = new Map<Id, Id>();
      // For SubAccountShare
      Map<Id, Id> repPairsSalesRepOnly = new Map<Id, Id>();
      // For SubAccountShare
      Map<Id, Id> repPairsAgencyRepOnly = new Map<Id, Id>();
      // For Tasks and Events;
      Map<Id, Id> repPairsAll = new Map<Id, Id>();
      // For Contacts:
      Map<Id, Id> contactPairs = new Map<Id, Id>();
      // Currency pair for opportunity;
      Map<String, String> currencyPairs = new Map<String, String>();

      // Update pairs maps
      if (updatedSubAccount.OwnerId != oldSubAccount.OwnerId) {
        repPairsOwnerOnly.put(oldSubAccount.OwnerId, updatedSubAccount.OwnerId);
      }
      if (updatedSubAccount.a2a_Sales_Rep__c != oldSubAccount.a2a_Sales_Rep__c) {
        repPairsSalesRepOnly.put(oldSubAccount.a2a_Sales_Rep__c, updatedSubAccount.a2a_Sales_Rep__c);
        repPairsAll.put(oldSubAccount.a2a_Sales_Rep__c, updatedSubAccount.a2a_Sales_Rep__c);
      }
      if (updatedSubAccount.a2a_Strategic_Rep__c != oldSubAccount.a2a_Strategic_Rep__c) {
        repPairsStrategicRepOnly.put(oldSubAccount.a2a_Strategic_Rep__c, updatedSubAccount.a2a_Strategic_Rep__c);
        repPairsAll.put(oldSubAccount.a2a_Strategic_Rep__c, updatedSubAccount.a2a_Strategic_Rep__c);
      }
      if (updatedSubAccount.a2a_Service_Rep__c != oldSubAccount.a2a_Service_Rep__c) {
        repPairsServiceRepOnly.put(oldSubAccount.a2a_Service_Rep__c, updatedSubAccount.a2a_Service_Rep__c);
        repPairsAll.put(oldSubAccount.a2a_Service_Rep__c, updatedSubAccount.a2a_Service_Rep__c);
      }
      if (updatedSubAccount.a2a_Agency_Rep__c != oldSubAccount.a2a_Agency_Rep__c) {
        repPairsAgencyRepOnly.put(oldSubAccount.a2a_Agency_Rep__c, updatedSubAccount.a2a_Agency_Rep__c);
        repPairsAll.put(oldSubAccount.a2a_Agency_Rep__c, updatedSubAccount.a2a_Agency_Rep__c);
      }
      if (updatedSubAccount.a2a_Primary_Contact__c != oldSubAccount.a2a_Primary_Contact__c) {
        contactPairs.put(oldSubAccount.a2a_Primary_Contact__c, updatedSubAccount.a2a_Primary_Contact__c);
        newContactIdToSubAccountOwnerId.put(updatedSubAccount.a2a_Primary_Contact__c, updatedSubAccount.OwnerId);
      }
      if (updatedSubAccount.CurrencyIsoCode != oldSubAccount.CurrencyIsoCode) {
        currencyPairs.put(oldSubAccount.CurrencyIsoCode, updatedSubAccount.CurrencyIsoCode);
      }

      if (!repPairsOwnerOnly.isEmpty()) {
        subAccountIdToChangedOwnerPairs.put(updatedSubAccount.Id, repPairsOwnerOnly);
      }
      if (!repPairsAll.isEmpty()) {
        subAccountIdToChangedAllRepPairs.put(updatedSubAccount.Id, repPairsAll);
      }
      if (!contactPairs.isEmpty()) {
        subAccountIdToChangedContactsPair.put(updatedSubAccount.Id, contactPairs);
      }
      if (!repPairsServiceRepOnly.isEmpty()) {
        subAccountIdToChangedServiceRepPairs.put(updatedSubAccount.Id, repPairsServiceRepOnly);
      }
      if (!repPairsStrategicRepOnly.isEmpty()) {
        subAccountIdToChangedStrategicRepPairs.put(updatedSubAccount.Id, repPairsStrategicRepOnly);
      }
      if (!repPairsSalesRepOnly.isEmpty()) {
        subAccountIdToChangedSalesRepPairs.put(updatedSubAccount.Id, repPairsAgencyRepOnly);
      }
      if (!currencyPairs.isEmpty()) {
        subAccountIdToChangedCurrencyPairs.put(updatedSubAccount.Id, currencyPairs);
      }
    }
  }

  private void updateOwnersForRelatedObjectsAfterUpdate(Account[] newSubAccounts) {
    update A2A_SubAccountTriggerOwnerChangeLists.getOpportunitiesToUpdate(subAccountIdToChangedOwnerPairs);
    update A2A_SubAccountTriggerOwnerChangeLists.getContactsToUpdate(
        subAccountIdToChangedContactsPair, newContactIdToSubAccountOwnerId,
        subAccountIdToChangedOwnerPairs, newSubAccounts);
    List<Case> updatedCasesList = A2A_SubAccountTriggerOwnerChangeLists.getCasesToUpdate(subAccountIdToChangedServiceRepPairs);

    if (!updatedCasesList.isEmpty()) {
      update updatedCasesList;
    }

    // custom setting to configure this
    Async_Account_Processor_Setting__mdt setting = [SELECT Id, Is_Sync_Processing_Enabled__c FROM Async_Account_Processor_Setting__mdt LIMIT 1];
    Boolean isTasksProcessedInSync = setting.Is_Sync_Processing_Enabled__c;

    if(isTasksProcessedInSync){
      updateTasksInSync();
    }else{
      updateTasksInAsync();
    }

  }

  private void updateTasksInAsync(){
    try{
      Async_Request__c req = new Async_Request__c(Async_Type__c='FetchAccountRelatedObjects');
      req.Parameters__c = JSON.serialize(subAccountIdToChangedAllRepPairs);
      insert req;
    }catch(Exception e){
      //throw error : unable to process task records
    }

  }

  private void updateTasksInSync(){
    List<SObject> activitiesToUpdate = A2A_SubAccountTriggerOwnerChangeLists.getUpdatedTasksAndEvents(subAccountIdToChangedAllRepPairs);
    List<SObject> nonRecurringActivities = filterNonRecurringActivities(activitiesToUpdate);
    update nonRecurringActivities;

    // Salesforce doesn't allow bulk insert/update/delete recurring tasks/events, have to update 1 by 1
    List<SObject> recurringActivities = filterRecurringActivities(activitiesToUpdate);
    for (SObject task : recurringActivities) {
      update task;
    }
  }


  private void updateCurrencyForRelatedOpportunitiesAfterUpdate() {
    if (!subAccountIdToChangedCurrencyPairs.isEmpty()) {
      List<Opportunity> opportunities = A2A_SubAccountTriggerCurrencyChangeLists.getOpportunitiesToUpdate(subAccountIdToChangedCurrencyPairs);
      if (opportunities.size() > 0) {
        opportunityLineItemUtility.updateOpportunities(opportunities);
      }
    }
  }

  private List<SObject> filterNonRecurringActivities(List<SObject> activities) {
    return filterActivities(activities, false);
  }

  private List<SObject> filterRecurringActivities(List<SObject> activities) {
    return filterActivities(activities, true);
  }

  private List<SObject> filterActivities(List<SObject> activities, Boolean filterRecurring) {
    List<SObject> results = new List<SObject>();
    for (SObject activity : activities) {
      if (activity.get('IsRecurrence') == filterRecurring) {
        results.add(activity);
      }
    }
    return results;
  }

  private static void updateAccountTeamMembers(List<Account> newList, Map<Id, Account> oldMap) {
    Set<Id> updatedAccountIdSet = new Set<Id>();

    for (Account account : newList) {
      if (account.ParentId == null) {
        continue;
      }
      Account oldAccount = oldMap.get(account.Id);

      if (oldAccount.A2A_Sales_Rep__c != account.A2A_Sales_Rep__c ||
        oldAccount.A2A_Service_Rep__c != account.A2A_Service_Rep__c ||
        oldAccount.A2A_Strategic_Rep__c != account.A2A_Strategic_Rep__c ||
        oldAccount.A2A_Agency_Rep__c != account.A2A_Agency_Rep__c) {

        updatedAccountIdSet.add(account.Id);
      }
    }
    if (!updatedAccountIdSet.isEmpty()) {
      A2A_AccountTeamMemberManagement.syncAccountTeamMembers(updatedAccountIdSet);
    }
  }

  private void updateParentAccountOwnerCheckbox(List<Account> accountList,
    Map<Id, Account> oldAccountMap) {

    Map<Id, Account> parentAccountOwnerMap = new Map<Id, Account>();

    for (Account account : accountList) {
      if (account.ParentId == null) {
        continue;
      }
      // for update event
      if (oldAccountMap != null) {
          Account oldAccount = oldAccountMap.get(account.Id);

          if (account.a2a_Salesforce_Mastered__c &&
             (account.recordTypeId == nationalSubAccountRecTypeId &&
              account.a2a_Sales_Rep__c != oldAccount.a2a_Sales_Rep__c) ||
              (account.recordTypeId == agencySubAccountRecTypeId &&
                  account.a2a_Agency_Rep__c != oldAccount.a2a_Agency_Rep__c)) {

              if (!parentAccountOwnerMap.containsKey(account.ParentId)) {
                  parentAccountOwnerMap.put(account.ParentId, new Account(
                    Id = account.ParentId, a2a_Needs_Owner_Update__c = true));
              }
        }
      // for insert event
      } else if ((account.recordTypeId == nationalSubAccountRecTypeId &&
            account.a2a_Sales_Rep__c != null) ||
            (account.recordTypeId == agencySubAccountRecTypeId &&
                account.a2a_Agency_Rep__c != null)) {

            if (!parentAccountOwnerMap.containsKey(account.ParentId)) {
                parentAccountOwnerMap.put(account.ParentId, new Account(
                    Id = account.ParentId, a2a_Needs_Owner_Update__c = true));
            }
      }
    }
    if (!parentAccountOwnerMap.isEmpty()) {
        update parentAccountOwnerMap.values();
    }
  }

  private void updateOpportunityReps(List<Account> updatedSubAccounts, Map<Id, Account> oldSubAccountsMap) {
    Map<Id, Id> salesRepsByAccountIds = new Map<Id, Id>();
    Map<Id, Id> agencyRepsByAccountIds = new Map<Id, Id>();

    getOpportunityRepsToUpdate(updatedSubAccounts, oldSubAccountsMap, salesRepsByAccountIds, agencyRepsByAccountIds);

    if(agencyRepsByAccountIds.isEmpty() && salesRepsByAccountIds.isEmpty()) {
      return;
    }
    List<Opportunity> oppsToUpdate = new List<Opportunity>();
    List<OpportunityTeamMember> oppTeamToInsert = new List<OpportunityTeamMember>();

    List<Opportunity> opps = [select Id, Sales_Rep__c, Agency_Rep__c, AccountId
                              from Opportunity
                              where AccountId in :updatedSubAccounts
                              and isClosed != true];

    for(Opportunity opp : opps) {

      if(salesRepsByAccountIds.keySet().contains(opp.AccountId)) {
        opp.Sales_Rep__c = salesRepsByAccountIds.get(opp.AccountId);
        oppsToUpdate.add(opp);
        oppTeamToInsert.add(A2A_Utility.addOpportunityTeamMembers(opp.Id, opp.Sales_Rep__c, Constants.SALES_REP_ROLE));
      }
      if(agencyRepsByAccountIds.keySet().contains(opp.AccountId)) {
        opp.Agency_Rep__c = agencyRepsByAccountIds.get(opp.AccountId);

        if(!oppsToUpdate.contains(opp))
          oppsToUpdate.add(opp);
        oppTeamToInsert.add(A2A_Utility.addOpportunityTeamMembers(opp.Id, opp.Agency_Rep__c, Constants.AGENCY_REP_ROLE));
      }
    }
    try {
      insert oppTeamToInsert;
      update oppsToUpdate;
    } catch(Exception e) {
      apexLogHandler.apexLog log = new apexLogHandler.apexLog(
        'A2A_AccountTriggerHandler', 'updateOpportunityReps()', e.getMessage());
    }
  }

  private void getOpportunityRepsToUpdate(List<Account> updatedSubAccounts, Map<Id, Account> oldSubAccountsMap,
    Map<Id, Id> salesRepsByAccountIds, Map<Id, Id> agencyRepsByAccountIds) {

    for(Account subAcc : updatedSubAccounts) {
      Account oldSubAcc = oldSubAccountsMap.get(subAcc.Id);

      if (oldSubAcc != null) {

        if((subAcc.a2a_Sales_Rep__c != null && subAcc.a2a_Sales_Rep__c != Label.Non_pilot_userId) &&
           (oldSubAcc.a2a_Sales_Rep__c != subAcc.a2a_Sales_Rep__c)) {

          salesRepsByAccountIds.put(subAcc.Id, subAcc.a2a_Sales_Rep__c);
        }
        if((subAcc.a2a_Agency_Rep__c != null && subAcc.a2a_Agency_Rep__c != Label.Non_pilot_userId) &&
           (oldSubAcc.a2a_Agency_Rep__c != subAcc.a2a_Agency_Rep__c)) {

          agencyRepsByAccountIds.put(subAcc.Id, subAcc.a2a_Agency_Rep__c);
        }
      }
    }
  }
}